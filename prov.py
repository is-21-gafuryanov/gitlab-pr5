#Задание 1

file = open("hello.txt", "w")

file.write("Hello, World!")

file.close()

file = open("hello.txt", "r")

content = file.read()

print(content)

file.close()

#Задание 2
file = open("sentences.txt", "w")

sentences = ["Привет, как дела?",
             "Что нового?",
             "Как проходит день?"]


for sentence in sentences:
    file.write(sentence)


file.close()

file = open('sentences.txt' ,'r')

content = file.read()

print(content)

file.close()